//
//  FileViewController.swift
//  PreviewController
//
//  Created by  Сергей on 21.01.17.
//  Copyright © 2017  Сергей. All rights reserved.
//

import Foundation
import QuickLook

class FileViewController: QLPreviewController, QLPreviewControllerDataSource, QLPreviewControllerDelegate {
    
    var files : [File]
    
    init(withFiles files: [File] ) {
        self.files = files
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.dataSource = self
        self.delegate = self
    }
    
    //dataSource
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return self.files.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return files[index]
    }
    
    //delegate
    
    
    
    
    
}
