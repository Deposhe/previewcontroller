//
//  File.swift
//  PreviewController
//
//  Created by  Сергей on 21.01.17.
//  Copyright © 2017  Сергей. All rights reserved.
//

import Foundation
import QuickLook

class File : NSObject, QLPreviewItem {
    
    var fileURL : URL
    
    init(withURL fileURL: URL) {
        self.fileURL = fileURL
        super.init()
    }
    
    var previewItemURL: URL? {
        return fileURL
    }
    
    var previewItemTitle: String? {
        return fileURL.lastPathComponent
    }
}
