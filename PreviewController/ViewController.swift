//
//  ViewController.swift
//  PreviewController
//
//  Created by  Сергей on 21.01.17.
//  Copyright © 2017  Сергей. All rights reserved.
//

import UIKit
import QuickLook
import DKImagePickerController
import Photos

class ViewController: UIViewController {

    var files = [File]()
    var sellectedAssets = [DKAsset]() {
        didSet {
            self.prepareAssets()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fileNames = ["file1.pdf", "file2.pdf", "file3.xlsx"]
        
        //files
        
        
        
        //files generator
        for filename in fileNames {
        
            let path = Bundle.main.url(forResource: filename, withExtension: nil)
            print(path ?? "NO path")
            
            files.append(File(withURL: path!))
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didPressShowButton(_ sender: Any) {
        
        let controller = FileViewController(withFiles: self.files)
        controller.currentPreviewItemIndex = 2
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func didPressShowPicker(_ sender: Any) {
        
        let picker = DKImagePickerController()
        picker.showsCancelButton = true
        picker.didSelectAssets = { (assets: [DKAsset]) in

            self.sellectedAssets = assets
        }
        self.present(picker, animated: true, completion: nil)
        

    }
    
    func prepareAssets() {
        
        for asset in self.sellectedAssets {
            
            let phAsset = asset.originalAsset

            phAsset?.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (input, info) in
                
                let urls = self.files.map({ (file) -> URL in
                    return file.fileURL
                })
                
                if let imageUrl = input?.fullSizeImageURL {
                    
                    if !urls.contains(imageUrl) {
                        
                        let file = File(withURL: imageUrl)
                        self.files.append(file)
                    }
                }
            })
        }
    }
}



